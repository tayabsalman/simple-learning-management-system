<?php
require_once("entity/entity.php");
require_once("system/enums.php");
require_once("system/exceptions.php");

class courseEntity extends Entity
{
    public function __construct()
    {
        parent::__construct();
    }

    public function ListAll($from = 0, $count = 0, $search = "")
    {
        $query = "SELECT `id`, `title`, `validTill`, `published`, `createdOn` FROM `Course` WHERE `deleted`='0' ";
        if(strlen($search) > 0)
            $query .= "AND `title` LIKE '%" . $this->Escape($search) . "%' ";
        
        $query .= "ORDER BY `id` DESC";
        if($from >= 0 && $count >0)
            $query .= " LIMIT ". $this->Escape($from) .", ". $this->Escape($count);
        
        $this->Query($query);
        $result = array();
        $courseIds = array();
        while($row = $this->GetItem())
        {
            $courseIds[$row["id"]] = $row["id"];
            $row["lessonCount"] = 0;
            $row["published"] = YesorNo::Str[$row["published"]];

            $result[$row["id"]] = $row;
        }
        if(count($courseIds) > 0)
        {
            $query = "SELECT `CourseLesson`.`courseId` as courseId, COUNT(`CourseLesson`.`lessonId`) as lessonCount FROM `Lesson` ".
                     "INNER JOIN `CourseLesson` ON `CourseLesson`.`courseId` IN('". implode("', '", array_keys($courseIds)) ."') AND `Lesson`.`id` = `CourseLesson`.`lessonId` ".
                     "WHERE `CourseLesson`.`deleted`='0' AND `Lesson`.`deleted`='0' ".
                     "GROUP BY `CourseLesson`.`courseId`";
            $this->Query($query);
            while($row = $this->GetItem())
                $result[$row["courseId"]]["lessonCount"] = $row["lessonCount"];
        }
        return array_values($result);
    }

    public function Count($search = "")
    {
        $query = "SELECT COUNT(`id`) as count FROM `Course` WHERE `deleted`='0'";
        if(strlen($search) > 0)
            $query .= " AND `title` LIKE '%" . $this->Escape($search) . "%'";
        
        $this->Query($query);
        $count = 0;
        if($row = $this->GetItem())
            $count = $row["count"];
        return $count;
    }

    public function IsUrlAlreadyInUse($action = ItemAction::NEW)
    {
        $query = "SELECT `id` FROM `URLs` WHERE `urlKey` = '".$this->Escape($this->url). "'";

        if($action == ItemAction::UPDATE)
            $query .= " AND `action` !='course&id=".$this->Escape($this->id) . "'";
        $this->Query($query);
        if($row = $this->GetItem())
            return true;
        return false;
    }

    public function CreateNewCourse()
    {
        $query = "INSERT INTO `Course`(`title`, `description`, `validTill`, `published`) ".
                 "VALUES('". $this->Escape($this->title) ."', '". $this->Escape($this->description) .
                 "', '". $this->Escape($this->validTill) ."', '". $this->Escape($this->published) ."')";
        $this->Query($query);
        if($this->GetQueryResult())
        {
            $this->id = $this->GetGeneratedId();
            $this->SetCourseLessons();
            
            $query = "INSERT INTO `URLs`(`urlKey`, `action`) ".
                     "VALUES('". $this->Escape($this->url) ."', 'course&id=". $this->Escape($this->id) ."')";
            $this->Query($query);
        }
    }
    
    public function SetCourseLessons()
    {
        $query = "UPDATE `CourseLesson` SET `deleted`='1' WHERE `courseId` = '" . $this->Escape($this->id)."'";
        $this->Query($query);

        if(count($this->lessons) > 0)
        {
            $values = array();
            foreach($this->lessons as $index => $lessonId)
                $values[] = "('".$this->Escape($this->id)."', '". $this->Escape($lessonId) ."', '0')";
            $query = "REPLACE INTO `CourseLesson`(`courseId`, `lessonId`, `deleted`) ".
                     "VALUES". implode(", ", $values);
            $this->Query($query);
            if(!$this->GetQueryResult())
            {
                $this->Close();
                throw new InvalidDataException("Couldn't set course lessons");
            }
        }
    }

    public function IsLatest($lastUpdatedOn)
    {
        $query = "SELECT `id` FROM `Course` WHERE `Id`='". $this->Escape($this->id) ."' ".
                 "AND `deleted` = '0' AND `updatedOn` = '". $this->Escape($lastUpdatedOn) ."'";
        $this->Query($query);
        if($row = $this->GetItem())
            return true;
        return false;
    }

    public function UpdateCourse()
    {
        $query = "UPDATE `Course` SET `title`='". $this->Escape($this->title) ."', ".
                 "`description`='". $this->Escape($this->description) ."', ".
                 "`validTill`='". $this->Escape($this->validTill) ."', ".
                 "`published`='". $this->Escape($this->published) ."'".
                 " WHERE `id` = '". $this->Escape($this->id) ."' AND `deleted`='0'";
        $this->Query($query);
        if($this->GetQueryResult())
        {
            $this->SetCourseLessons();
            
            $query = "UPDATE `URLs` SET `urlKey`='". $this->Escape($this->url) ."' ".
                     "WHERE `action` = 'course&id=". $this->Escape($this->id) . "' ";
            $this->Query($query);
            if(!$this->GetQueryResult())
            {
                $this->Close();
                throw new InvalidDataException("Couldn't update course url");
            }
        }
    }

    public function GetCourseInfo()
    {
        $query = "SELECT * FROM `Course` WHERE `id`='". $this->Escape($this->id) ."' AND `deleted`='0'";
        $this->Query($query);
        if($row = $this->GetItem())
            return $row;
        return array();
    }

    public function GetCompleteData()
    {
        $data = $this->GetCourseInfo();
        if($data)
        {
            $data["lessons"] = $this->GetCourseLessons();
            $data["url"] = $this->GetUrl();
        }
        return $data;
    }

    public function GetCourseLessons()
    {
        $lessons = array();
        $query = "SELECT `id`, `title` FROM `Lesson` ".
                 "INNER JOIN `CourseLesson` ON `CourseLesson`.`courseId` = '". $this->Escape($this->id) ."' AND `CourseLesson`.`lessonId` = `Lesson`.`id` ".
                 "WHERE `Lesson`.`deleted`='0' AND `CourseLesson`.`deleted`='0'";
        $this->Query($query);
        while($row = $this->GetItem())
            $lessons[] = $row;
        return $lessons;
    }

    public function GetUrl()
    {
        $query = "SELECT `urlKey` as url FROM `URLs` ".
                 "WHERE `action` = 'course&id=". $this->Escape($this->id) ."' AND `isDirty`='0'";
        $this->Query($query);
        if($row = $this->GetItem())
            return $row["url"];
        return "";
    }

    public function GetCourseList()
    {
        $query = "SELECT `Course`.`id`, `Course`.`title`, `Course`.`validTill`, `URLs`.`urlKey` FROM `Course` ".
                 "INNER JOIN `URLs` ON `URLs`.`action` = CONCAT('course&id=', `Course`.`id`) ".
                 " WHERE `Course`.`deleted`='0' AND `Course`.`published`='1' AND `Course`.`validTill`>=CURRENT_DATE ORDER BY `id`";
        $this->Query($query);

        $result = array();
        $courseIds = array();
        while($row = $this->GetItem())
        {
            $row["title"] = (strlen($row["title"]) > 40)? substr($row["title"], 0, 40) . "..." : $row["title"];
            $validityParts = explode("-", explode(" ", $row["validTill"])[0]);
            $row["validTill"] = implode("-", array_reverse($validityParts));

            $result[$row["id"]] = $row;
            $courseIds[$row["id"]] = $row["id"];
        }

        if(count($courseIds) > 0)
        {
            $query = "SELECT `CourseLesson`.`courseId` as courseId, COUNT(`CourseLesson`.`lessonId`) as lessonCount FROM `Lesson` ".
                     "INNER JOIN `CourseLesson` ON `CourseLesson`.`courseId` IN('". implode("', '", array_keys($courseIds)) ."') AND `Lesson`.`id` = `CourseLesson`.`lessonId` ".
                     "WHERE `CourseLesson`.`deleted`='0' AND `Lesson`.`deleted`='0' AND `Lesson`.`published`='1' ".
                     "GROUP BY `CourseLesson`.`courseId`";
            $this->Query($query);
            while($row = $this->GetItem())
                $result[$row["courseId"]]["lessonCount"] = $row["lessonCount"];
        }
        return $result;
    }

    public function GetPage()
    {
        $data = array();
        $query = "SELECT * FROM `Course` WHERE `id`='". $this->Escape($this->id) ."' AND `Course`.`published`='1' AND `deleted`='0' AND `Course`.`validTill`>=CURRENT_DATE";
        $this->Query($query);
        if($row = $this->GetItem())
        {
            $validityParts = explode("-", explode(" ", $row["validTill"])[0]);
            $row["validTill"] = implode("-", array_reverse($validityParts));

            $data["title"] = $row["title"];
            $data["validTill"] = $row["validTill"];
            $data["description"] = $row["description"];

            $query = "SELECT `Lesson`.`id`, `Lesson`.`title`, `Lesson`.`type`, `URLs`.`urlKey` FROM `CourseLesson` ".
                     "INNER JOIN `Lesson` ON `CourseLesson`.`courseId`='". $this->Escape($this->id) ."' AND `Lesson`.`id`=`CourseLesson`.`lessonId` ".
                     "INNER JOIN `URLs` ON `URLs`.`action`=CONCAT('lesson&id=', `CourseLesson`.`lessonId`) ".
                     "WHERE `CourseLesson`.`deleted`='0' AND `Lesson`.`deleted`='0' AND `Lesson`.`published`='1' AND `URLs`.`isDirty`='0'";
            
            $this->Query($query);
            $data["lessons"] = array();
            $lessonCount = 0;
            while($row = $this->GetItem())
            {
                $row["title"] = (strlen($row["title"]) > 40)? substr($row["title"], 0, 40): $row["title"];
                $row["type"] = LessonType::Str[$row["type"]];
                $data["lessons"][$row["id"]] = $row;
                $lessonCount++;
            }
            $data["lessonCount"] = $lessonCount;
        }
        return $data;
    }
}