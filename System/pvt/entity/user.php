<?php
require_once("entity/entity.php");
require_once("system/exceptions.php");

class userEntity extends Entity
{
    public function __construct()
    {
        parent::__construct();
    }

    public function ListAll($from = 0, $count = 0, $search = "")
    {
        $query = "SELECT `id`, `name`, `email`, `isAdmin`, `createdOn` FROM `Users` WHERE `deleted`='0' ";
        if(strlen($search) > 0)
            $query .= "AND `email` LIKE '%" . $this->Escape($search) . "%' ";
        
        $query .= " ORDER BY `id` DESC";
        
        if($count >= 0 && $count > 0)
            $query .= " LIMIT ". $this->Escape($from) .", ". $this->Escape($count);
        
        $this->Query($query);
        $result = array();
        while($row = $this->GetItem())
        {
            $row["role"] = UserType::Str[$row["isAdmin"]];
            $result[] = $row;
        }
        return $result;
    }

    public function Count($search = "")
    {
        $query = "SELECT COUNT(`id`) as count FROM `Users` WHERE `deleted`='0'";
        if(strlen($search) > 0)
            $query .= " AND `email` LIKE '%" . $this->Escape($search) . "%'";
        
        $this->Query($query);
        $count = 0;
        if($row = $this->GetItem())
            $count = $row["count"];
        return $count;
    }
    
    public function Authenticate($passwd)
    {
        $query = "SELECT * FROM `Users` WHERE `email` = '". $this->Escape($this->email). "' ".
                 "AND `deleted` = '0'";
        $this->query($query);
        if($row = $this->GetItem())
            if($row["password"] == $passwd)
                return ;
        
        $this->Close();
        throw new AuthException("Either username or password is invalid");
    }

    public function GetUserData()
    {
        $data = array();
        $query = "SELECT * FROM `Users` WHERE `email` = '". $this->Escape($this->email). "' ".
                 "AND `deleted` = '0'";
        $this->query($query);
        if($row = $this->GetItem())
        {
            $data["id"] = $row["id"];
            $data["name"] = $row["name"];
            $data["email"] = $row["email"];
            $data["isAdmin"] = $row["isAdmin"];
        }
        return $data;
    }

    public function RegisterNewUser()
    {
        $query = "SELECT * FROM `Users` WHERE `email` = '". $this->Escape($this->email). "' ".
                 "AND `deleted` = '0'";
        $this->query($query);
        if($row = $this->GetItem())
        {
            $this->Close();
            throw new AuthException("Email Already Registered.");
        }
        
        $userData = array();
        $query = "INSERT INTO `Users`(`name`, `email`, `password`) ".
                 "VALUES('". $this->Escape($this->name) ."', '". $this->Escape($this->email) ."', '". $this->Escape($this->passwd) ."')";
        $this->query($query);
        if($row = $this->GetQueryResult())
        {
            $userData["id"] = $this->GetGeneratedId();
            $userData["name"] = $this->name;
            $userData["email"] = $this->email;
        }
        return $userData;
    }
}