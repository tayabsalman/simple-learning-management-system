<?php
require_once("entity/entity.php");
require_once("system/enums.php");
require_once("system/exceptions.php");

class lessonEntity extends Entity
{
    public function __construct()
    {
        parent::__construct();
    }

    public function ListAll($from = 0, $count = 0, $search = "")
    {
        $query = "SELECT `id`, `title`, `type`, `published`, `createdOn` FROM `Lesson` WHERE `deleted`='0' ";
        if(strlen($search) > 0)
            $query .= "AND `title` LIKE '%" . $this->Escape($search) . "%' ";
        
        $query .= " ORDER BY `id` DESC";
        
        if($count >= 0 && $count > 0)
            $query .= " LIMIT ". $this->Escape($from) .", ". $this->Escape($count);
        
        $this->Query($query);
        $result = array();
        while($row = $this->GetItem())
        {
            $row["type"] = LessonType::Str[$row["type"]];
            $row["published"] = YesorNo::Str[$row["published"]];
            $result[] = $row;
        }
        return $result;
    }

    public function Count($search = "")
    {
        $query = "SELECT COUNT(`id`) as count FROM `Lesson` WHERE `deleted`='0'";
        if(strlen($search) > 0)
            $query .= " AND `title` LIKE '%" . $this->Escape($search) . "%'";
        
        $this->Query($query);
        $count = 0;
        if($row = $this->GetItem())
            $count = $row["count"];
        return $count;
    }

    public function IsUrlAlreadyInUse($action = ItemAction::NEW)
    {
        $query = "SELECT `id` FROM `URLs` WHERE `urlKey` = '".$this->Escape($this->url). "'";

        if($action == ItemAction::UPDATE)
            $query .= " AND `action` !='lesson&id=".$this->Escape($this->id) . "'";
        $this->Query($query);
        if($row = $this->GetItem())
            return true;
        return false;
    }

    public function CreateNewLesson()
    {
        $query = "INSERT INTO `Lesson`(`title`, `type`, `description`, `published`) ".
                 "VALUES('". $this->Escape($this->title) ."', '". $this->Escape($this->type) .
                 "', '". $this->Escape($this->description) ."', '". $this->Escape($this->published) ."')";
        $this->Query($query);
        if($this->GetQueryResult())
        {
            $this->id = $this->GetGeneratedId();
            $this->SetLessonData();
            
            $query = "INSERT INTO `URLs`(`urlKey`, `action`) ".
                     "VALUES('". $this->Escape($this->url) ."', 'lesson&id=". $this->Escape($this->id) ."')";
            $this->Query($query);
        }
    }
    
    public function SetLessonData()
    {
        //Update db schema and remove id and make primary keys from LessonData table and add deleted column 
        $query = "UPDATE `LessonData` SET `deleted`='1' WHERE `lessonId` = '". $this->Escape($this->id) ."'";
        $this->Query($query);

        $query = "REPLACE INTO `LessonData`(`lessonId`, `contentType`, `data`, `deleted`) ";
        
        $query .= "VALUES ('". $this->escape($this->id) ."', '". $this->Escape($this->type) ."', '". $this->Escape(json_encode($this->data)) ."', '0')";
        $this->Query($query);
        if(!$this->GetQueryResult())
        {
            $this->Close();
            throw new InvalidDataException("Couldn't set lesson data");
        }
    }

    public function IsLatest($lastUpdatedOn)
    {
        $query = "SELECT `id` FROM `Lesson` WHERE `id`='". $this->Escape($this->id) ."' ".
                 "AND `deleted` = '0' AND `updatedOn` = '". $this->Escape($lastUpdatedOn) ."'";
        $this->Query($query);
        if($row = $this->GetItem())
            return true;
        return false;
    }

    public function UpdateLesson()
    {
        $query = "UPDATE `Lesson` SET `title`='". $this->Escape($this->title) ."', ".
                 "`description`='". $this->Escape($this->description) ."', ".
                 "`published`='". $this->Escape($this->published) ."'".
                 " WHERE `id` = '". $this->Escape($this->id) ."' AND `deleted`='0'";
        $this->Query($query);
        if($this->GetQueryResult())
        {
            $this->SetLessonData();
            
            $query = "UPDATE `URLs` SET `urlKey`='". $this->Escape($this->url) ."' ".
                     "WHERE `action` = 'lesson&id=". $this->Escape($this->id) . "' ";
            $this->Query($query);
            if(!$this->GetQueryResult())
            {
                $this->Close();
                throw new InvalidDataException("Couldn't update lesson url");
            }
        }
    }

    public function SaveAttachmentUploadInfo($uploadedFile)
    {
        $attachments = array();
        $query = "SELECT `data` FROM `LessonData` where `lessonId`='". $this->Escape($this->id) ."' AND `deleted`='0'";
        $this->Query($query);
        if($row = $this->GetItem())
            $attachments = json_decode($row["data"]);
        array_push($attachments, $uploadedFile);
        
        $query = "REPLACE INTO `LessonData`(`lessonId`, `contentType`, `data`, `deleted`) ".
                 "VALUES('". $this->Escape($this->id) ."', '". LessonType::ATTACHMENT ."', '". $this->Escape(json_encode($attachments)) ."', '0')";
        $this->Query($query);
        if($this->GetQueryResult())
            return true;
        return false;
    }

    public function GetCompleteData()
    {
        $data = $this->GetLessonInfo();
        $data["data"] = $this->GetLessonData();
        $data["url"] = $this->GetUrl();
        return $data;
    }

    public function GetLessonInfo()
    {
        $query = "SELECT * FROM `Lesson` WHERE `id`='". $this->Escape($this->id)."' AND `deleted`='0'";
        $this->Query($query);
        if($row = $this->GetItem())
            return $row;
        return array();
    }

    public function GetLessonData()
    {
        $query = "SELECT `data` FROM `LessonData` ".
                 "INNER JOIN `Lesson` ON `Lesson`.`id` = '". $this->Escape($this->id) ."' AND `Lesson`.`deleted` = '0'".
                 " AND `Lesson`.`id` = `LessonData`.`lessonId`";
        $this->Query($query);
        if($row = $this->GetItem())
            return json_decode($row["data"]);
        return array();
    }

    public function GetUrl()
    {
        $query = "SELECT `urlKey` as url FROM `URLs` ".
                 "WHERE `action` = 'lesson&id=". $this->Escape($this->id) ."' AND `isDirty`='0'";
        $this->Query($query);
        if($row = $this->GetItem())
            return $row["url"];
        return "";
    }

    public function GetAttachments()
    {
        $attachments = array();
        $query = "SELECT `data` as attachments FROM `LessonData` ".
                 "WHERE `lessonId`='". $this->Escape($this->id) ."'";
        if($row = $this->GetItem())
            $attachments = json_decode($row["attachments"]);
        return $attachments;
    }
    
    public function GetPage()
    {
        $data = array();
        $query = "SELECT `Lesson`.`id`, `Lesson`.`title`, `Lesson`.`type`, `Lesson`.`description`, `LessonData`.`data` FROM `Lesson` ".
                 "INNER JOIN `LessonData` ON `LessonData`.`lessonId` = `Lesson`.`id` AND `LessonData`.`deleted`='0' ".
                 "WHERE `Lesson`.`id`='". $this->Escape($this->id) ."' AND `Lesson`.`deleted`='0' AND `Lesson`.`published`='1'";
        $this->Query($query);
        if($row = $this->GetItem())
        {
            $data["id"] = $row["id"];
            $data["title"] = $row["title"];
            $data["type"] = $row["type"];
            $data["description"] = $row["description"];
            $row["data"] = json_decode($row["data"]);
            if($row["type"] == LessonType::VIDEO)
                $data["data"] = $row["data"];
            else
            {
                $data["data"] = array();
                foreach($row["data"] as $index=>$attachment)
                {
                    $parts = explode("/", $attachment);
                    $name = array_pop($parts);
                    array_push($data["data"], array("url" => $attachment, "name" => $name));
                }
            }
        }
        return $data;
    }
}