<?php
require_once("entity/entity.php");
class urlMgntEntity extends entity
{
    public $urlKey = "";
    public function __construct()
    {
        parent::__construct();
    }

    public function GetAction()
    {
        $action = -1;
        $query = "SELECT `action` FROM `URLs` ".
                 "WHERE `urlKey` = '".$this->Escape($this->urlKey)."' AND `isDirty`='0'";
        $this->Query($query);
        if($row = $this->GetItem())
            $action = $row["action"];
        return $action;
    }
}