<?php
require_once("system/enums.php");
require_once("/System/config/config.php");

class Authentication
{
    private static $permissionTable = array();

    public static function Permission($mod, $activity, $permission)
    {
        if(!isset(self::$permissionTable[$mod]))
            self::$permissionTable[$mod] = array();
        if(!isset(self::$permissionTable[$mod][$activity]))
            self::$permissionTable[$mod][$activity] = $permission;
    }

    public static function Start($isAdmin = false)
    {
        global $CONFIG;
        $session = $CONFIG["session"];

        $session_id = "";
        $key = ($isAdmin)? "admin": "user";
        if(!isset($_COOKIE[$session[$key]]))
        {
            $session_id = bin2hex(openssl_random_pseudo_bytes(32));
            setcookie($session[$key], $session_id);
        }
        $session_id = (strlen($session_id) == 0)? $_COOKIE[$session[$key]]: $session_id;

        //Keeping extra cookie make it difficult to get session key
        if(!isset($_COOKIE[$session["admin"]]))
            setcookie($session["admin"], bin2hex(openssl_random_pseudo_bytes(32)));
        if(!isset($_COOKIE[$session["user"]]))
            setcookie($session["user"], bin2hex(openssl_random_pseudo_bytes(32)));

        if(session_status() !== PHP_SESSION_ACTIVE)
        {
            session_name($session["name"]);
            session_id($session_id);
            session_start();
        }
    }

    public static function Validate($isAdmin = false)
    {
        $mod = $_REQUEST["mod"];
        $activity = $_REQUEST["activity"];
        if(isset(self::$permissionTable[$mod]) && isset(self::$permissionTable[$mod][$activity])
            && self::$permissionTable[$mod][$activity] == ControllerPermission::GUEST)
            return;
        
        self::Start($isAdmin);
        if(self::IsAuthenticated())
            return;
        throw new AuthException("Login Required");
    }

    public static function Authenticate($data = array(), $isAdmin = false)
    {
        self::Start($isAdmin);
        $_SESSION["data"] = $data;
        $_SESSION["authenticated"] = true;
    }

    public static function IsAuthenticated()
    {
        if(session_status() == PHP_SESSION_ACTIVE && isset($_SESSION["authenticated"]) && $_SESSION["authenticated"] == true)
            return true;
        return false;
    }
    
    public static function GetAuthData()
    {
        if(!self::IsAuthenticated())
            throw new AuthException("Un-Authorized user");
        return $_SESSION["data"];
    }

    public static function UnAuthenticate($isAdmin = false)
    {
        self::Start($isAdmin);
        session_destroy();
        $_SESSION = array();
    }
}