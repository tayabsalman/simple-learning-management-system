<?php

abstract class ControllerPermission
{
    const GUEST = 0;
    const ADMIN = 1;
}

abstract class ItemAction
{
    const NEW = 0;
    const UPDATE = 1;
}

abstract class LessonType
{
    const VIDEO = 0;
    const ATTACHMENT = 1;

    const Str = array(
        LessonType::VIDEO => "Video",
        LessonType::ATTACHMENT => "Attachments",
    );
}

abstract class UserType
{
    const USER = 0;
    const ADMIN = 1;

    const Str = array(
        UserType::USER => "User",
        UserType::ADMIN => "Admininstrator"
    );
}

abstract class YesorNo
{
    const NO = 0;
    const YES = 1;

    const Str = array(
        YesorNo::NO => "NO",
        YesorNo::YES => "YES"
    );
}