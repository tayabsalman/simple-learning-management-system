<?php
class Controller
{
    protected function GetRequiredParam($param)
    {
        if(!isset($_REQUEST[$param]))
            throw new MissingParamException("Missing Param : $param");
        return $_REQUEST[$param];
    }
    protected function GetOptionalParam($param, $optional = NULL)
    {
        if(isset($_REQUEST[$param]))
            return $_REQUEST[$param];
        return $optional;
    }
}
?>