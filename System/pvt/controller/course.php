<?php

require_once("controller/controller.php");
require_once("system/enums.php");
require_once("system/exceptions.php");
require_once("entity/course.php");

class courseController extends Controller
{
    public function listAck()
    {
        $from = $this->GetOptionalParam("from", 0);
        $count = $this->GetOptionalParam("count", 20);
        $search = $this->GetOptionalParam("searchStr", "");

        $courseEntity = new courseEntity();
        $data = $courseEntity->ListAll($from, $count, $search);
        $courseEntity->Close();
        return $data;
    }

    public function countAck()
    {
        $search = $this->GetOptionalParam("searchStr", "");

        $courseEntity = new courseEntity();
        $count = $courseEntity->Count($search);
        $courseEntity->Close();
        return $count;
    }

    public function addAck()
    {
        $id = $this->GetoptionalParam("id", -1);
        $title = $this->GetRequiredParam("title");
        $url = $this->GetRequiredParam("url");
        $description = $this->GetRequiredParam("description");
        $lessonIds = $this->GetOptionalParam("lessonIds", array());
        $validTill = $this->GetRequiredParam("validTill");
        $published = $this->GetOptionalParam("published", 0);

        $courseEntity = new courseEntity();
        $courseEntity->id = $id;
        $courseEntity->title = $title;
        $courseEntity->url = $url;
        $courseEntity->description = $description;
        $courseEntity->lessons = $lessonIds;
        $courseEntity->validTill = $validTill;
        $courseEntity->published = $published;
        if($id < 0)
        {
            if($courseEntity->IsUrlAlreadyInUse(ItemAction::NEW))
            {
                $courseEntity->Close();
                throw new InvalidDataException("Given URL is already in use, please change it and retry");
            }
            $courseEntity->CreateNewCourse();
        }
        else
        {
            $updatedOn = $this->GetRequiredParam("updatedOn");
            if(!$courseEntity->IsLatest($updatedOn))
            {
                $courseEntity->Close();
                throw new InvalidDataException("Take latest before saving");
            }
            if($courseEntity->IsUrlAlreadyInUse(ItemAction::UPDATE))
            {
                $courseEntity->Close();
                throw new InvalidDataException("Given URL is already in use, please change it and retry");
            }
            $courseEntity->UpdateCourse();
        }
        $data = $courseEntity->GetCourseInfo();
        $courseEntity->Close();
        return $data;
    }
    
    public function editAck()
    {
        $id = $this->GetRequiredParam("id");

        $courseEntity = new courseEntity();
        $courseEntity->id = $id;
        $data = $courseEntity->GetCompleteData();
        $courseEntity->Close();
        return $data;
    }
}