<?php
require_once("controller/controller.php");
require_once("system/enums.php");
require_once("entity/user.php");

class userController extends Controller
{
    public function listAck()
    {
        $from = $this->GetOptionalParam("from", 0);
        $count = $this->GetOptionalParam("count", 0);
        $search = $this->GetOptionalParam("searchStr", "");

        $userEntity = new userEntity();
        $data = $userEntity->ListAll($from, $count, $search);
        $userEntity->Close();
        return $data;
    }

    public function countAck()
    {
        $search = $this->GetOptionalParam("searchStr", "");

        $userEntity = new userEntity();
        $count = $userEntity->Count($search);
        $userEntity->Close();
        return $count;
    }

    public function authAck()
    {
        $email = $this->GetRequiredParam("email");
        $passwd = $this->GetRequiredParam("passwd");
        $isAdmin = $this->GetOptionalParam("type", false);

        //Should check for hack and prevent
        $userEntity = new userEntity();
        $userEntity->email = $email;
        $userEntity->Authenticate($passwd);

        $userData = $userEntity->GetUserData();
        $userEntity->Close();

        if($isAdmin && !$userData["isAdmin"])
            throw new AuthException("Given Email is not registered as Admin.");

        Authentication::Authenticate($userData,  $isAdmin);
        return $userData;
    }

    public function selfauthAck()
    {
        //Authenticating based on session data
        $userData = Authentication::GetAuthData();
        if($userData["isAdmin"] == 1)
            return $userData;
        throw new AuthException("Un authorized access");
    }

    public function newAck()
    {
        $name = $this->GetRequiredParam("name");
        $email = $this->GetRequiredParam("email");
        $passwd = $this->GetRequiredParam("passwd");

        $userEntity = new userEntity();
        $userEntity->name = $name;
        $userEntity->email = $email;
        $userEntity->passwd = $passwd;
        $userData = $userEntity->RegisterNewUser();
        $userEntity->Close();

        Authentication::Authenticate($userData);
        return $userData;
    }
    public function logoutAck()
    {
        Authentication::UnAuthenticate(true);
        return true;
    }
    public function userlogoutAck()
    {
        Authentication::UnAuthenticate();
        return true;
    }
}
Authentication::Permission("user", "new", ControllerPermission::GUEST);
Authentication::Permission("user", "auth", ControllerPermission::GUEST);
Authentication::Permission("user", "auth", ControllerPermission::GUEST);