<?php

require_once("controller/controller.php");
require_once("system/enums.php");
require_once("system/exceptions.php");
require_once("entity/lesson.php");

class lessonController extends Controller
{
    public function listAck()
    {
        $from = $this->GetOptionalParam("from", 0);
        $count = $this->GetOptionalParam("count", 0);
        $search = $this->GetOptionalParam("searchStr", "");

        $lessonEntity = new lessonEntity();
        $data = $lessonEntity->ListAll($from, $count, $search);
        $lessonEntity->Close();
        return $data;
    }

    public function countAck()
    {
        $search = $this->GetOptionalParam("searchStr", "");

        $lessonEntity = new lessonEntity();
        $count = $lessonEntity->Count($search);
        $lessonEntity->Close();
        return $count;
    }

    public function addAck()
    {
        $id = $this->GetoptionalParam("id", -1);
        $title = $this->GetRequiredParam("title");
        $type = $this->GetRequiredParam("type");
        $url = $this->GetRequiredParam("url");
        $description = $this->GetRequiredParam("description");
        if($type == LessonType::VIDEO)
            $data = $this->GetRequiredParam("embedVideoCode");
        else
        $data = $this->GetOptionalParam("attachments", array());
        $published = $this->GetOptionalParam("published", 0);

        $lessonEntity = new lessonEntity();
        $lessonEntity->id = $id;
        $lessonEntity->title = $title;
        $lessonEntity->type = $type;
        $lessonEntity->url = $url;
        $lessonEntity->description = $description;
        $lessonEntity->data = $data;
        $lessonEntity->published = $published;
        if($id < 0)
        {
            if($lessonEntity->IsUrlAlreadyInUse(ItemAction::NEW))
            {
                $lessonEntity->Close();
                throw new InvalidDataException("Given URL is already in use, please change it and retry");
            }
            $lessonEntity->CreateNewLesson();
        }
        else
        {
            $updatedOn = $this->GetRequiredParam("updatedOn");
            if(!$lessonEntity->IsLatest($updatedOn))
            {
                $lessonEntity->Close();
                throw new InvalidDataException("Take latest before saving");
            }
            if($lessonEntity->IsUrlAlreadyInUse(ItemAction::UPDATE))
            {
                $lessonEntity->Close();
                throw new InvalidDataException("Given URL is already in use, please change it and retry");
            }
            $lessonEntity->UpdateLesson();
        }
        $data = $lessonEntity->GetLessonInfo();
        $lessonEntity->Close();
        return $data;
    }

    public function editAck()
    {
        $id = $this->GetRequiredParam("id");

        $lessonEntity = new lessonEntity();
        $lessonEntity->id = $id;
        $data = $lessonEntity->GetCompleteData();
        $lessonEntity->Close();
        return $data;
    }

    public function getattachmentsAck()
    {
        $id = $this->GetRequiredParam("id");

        $lessonEntity = new lessonEntity();
        $lessonEntity->id = $id;
        $result = $lessonEntity->GetAttachments();
        $lessonEntity->Close();
        return $result;
    }
}