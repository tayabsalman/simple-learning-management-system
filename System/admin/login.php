<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Login | Admin Panel</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Play:400,700" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="theme/css/bootstrap.min.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="theme/style.css">
</head>

<body>
	<div class="error-pagewrap">
		<div class="error-page-int">
			<div class="text-center m-b-md custom-login">
				<h3>Hello, Administrator.</h3>
				<p>Use your admin credentials to get yourself verified!</p>
			</div>
			<div class="content-error">
				<div class="hpanel">
          <div class="panel-body">
              <form id="loginForm">
                <div class="form-group">
                    <label class="control-label" for="emailId">Email ID</label>
                    <input type="text" placeholder="Enter your Email ID" title="Please enter you Email ID" id="emailId" class="form-control">
                    <span class="help-block small js-email-help">Your unique username to app</span>
                </div>
                <div class="form-group">
                    <label class="control-label" for="password">Password</label>
                    <input type="password" title="Please enter your password" placeholder="******" id="password" class="form-control">
                    <span class="help-block small js-password-help">Your strong password</span>
                </div>
                <div class="form-group">
                    <label class="control-label hidden text-danger js-error-message">Error Message</label>
                </div>
                <button class="btn btn-success btn-block loginbtn">Login</button>
              </form>
          </div>
      </div>
			</div>
			<div class="text-center login-footer">
				<p>Copyright © 2020. All rights reserved. SLMS</a></p>
			</div>
		</div>   
    </div>
    <!-- jquery
		============================================ -->
    <script src="theme/js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="theme/js/bootstrap.min.js"></script>
    <script src="framework/framework.js"></script>
    <script>
      $(".loginbtn").unbind().bind("click", function(e){
        e.preventDefault();
        $(".js-email-help, .js-password-help, .js-error-message").addClass("hidden");
        var emailId = $("#emailId").val();
        var password = $("#password").val();
        var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (!re.test(emailId))
        {
          $(".js-email-help").removeClass("hidden").addClass("text-danger");
          $(".js-email-help").html("Enter a valid Email ID");
          return ;
        }
        if (password.trim().length < 6)
        {
          $(".js-password-help").removeClass("hidden").addClass("text-danger");
          $(".js-password-help").html("Enter your 6 digit password");
          return ;
        }

        Framework.Ajax("user", "auth", {email:emailId, passwd:password, type: true}, function(response){
          if(response.result.state == "ok")
            window.location.href = "/";
          else
            $(".js-error-message").removeClass("hidden").html(response.result.message);
        });

      });
    </script>
</body>

</html>