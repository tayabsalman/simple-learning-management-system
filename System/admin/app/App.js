function App(){
    this.node = null;
}

App.prototype.Init = function()
{
    Framework.LoadJS("common/constants.js", function(){
        this.Authenticate();
    }.bind(this));
}

App.prototype.Authenticate = function()
{
    Framework.Ajax(MOD_USER, USER_SELFAUTH, {}, function(response){
        if(response.result.state == "ok")
        {
            this.userData = response.data;
            this.BindEvents();
            this.refreshUI();
            this.MapHashLocation();
        }
        else
            window.location.href = "login.php";
    }.bind(this));
}

App.prototype.BindEvents = function()
{
    $("#all-lessons").unbind().bind("click", function(){
        Framework.LoadJS("common/List.js", function(){
            Framework.LoadJS("common/Elements.js", function(){
                Framework.LoadJS("common/Editor.js", function(){
                    $(this.node).empty();
                    var listObj = new List();
                    listObj.node = this.node;
                    listObj.controller = MOD_LESSON;
                    listObj.editor = "Lesson";
                    listObj.title = "Lesson";
                    listObj.pluralTitle = "Lessons"
                    listObj.Init();
                }.bind(this));
            }.bind(this));
        }.bind(this));
    }.bind(this));

    $("#all-courses").unbind().bind("click", function(){
        Framework.LoadJS("common/List.js", function(){
            Framework.LoadJS("common/Elements.js", function(){
                Framework.LoadJS("common/Editor.js", function(){
                    $(this.node).empty();
                    var listObj = new List();
                    listObj.node = this.node;
                    listObj.controller = MOD_COURSE;
                    listObj.editor = "Course";
                    listObj.title = "Course";
                    listObj.pluralTitle = "Courses"
                    listObj.Init();
                }.bind(this));
            }.bind(this));
        }.bind(this));
    }.bind(this));

    $("#all-users").unbind().bind("click", function(){
        Framework.LoadJS("common/List.js", function(){
            Framework.LoadJS("common/Elements.js", function(){
                $(this.node).empty();
                var listObj = new List();
                listObj.node = this.node;
                listObj.controller = MOD_USER;
                listObj.editor = "User";
                listObj.title = "User";
                listObj.pluralTitle = "Users"
                listObj.Init();
            }.bind(this));
        }.bind(this));
    }.bind(this));

    $(".js-logout").unbind().bind("click", function(){
        this.Logout();
    }.bind(this));
}
App.prototype.refreshUI = function()
{
    $(".js-admin-name").html(this.userData.name);
}
App.prototype.Logout = function()
{
    Framework.Ajax(MOD_USER, USER_LOGOUT, {}, function(response){
        if(response.result.state == "ok")
            window.location.href = "/";
        console.log(response);
    }.bind(this));
}
App.prototype.MapHashLocation = function()
{
    var hashLocation = window.location.hash;
    if(hashLocation.length > 1)
    {
        $(hashLocation).closest(".js-sidebar-element").find("a:first").trigger("click");
        $(hashLocation).trigger("click");
    }
}