function Course()
{
    this.node = null;
    this.id = -1;

    this.title = null;
    this.url = null;
    this.description = null;
    this.published = null;
    this.updatedOn = null;

    this.elementObj = null;
}

Course.prototype.Init = function()
{
    this.elementObj = new Elements();
    
    columns = this.elementObj.CreateTwoColumns();
    this.elementObj.CreateInputTextField($(columns).find('.column1'), "Title");
    this.title = $(columns).find(".column1 input");
    this.elementObj.CreateInputTextField($(columns).find('.column2'), "URL Key");
    this.url = $(columns).find(".column2 input");
    $(columns).appendTo(this.node);

    columns = this.elementObj.CreateTwoColumns();
    this.description = this.elementObj.CreateHTMLField($(columns).find('.column1'), "CourseDescription", "Description");

    var details = {
        parent: $(columns).find(".column2"),
        id: "course-lessons",
        label: "Add Lessons",
        placeholder: "Select lessons",
        controller: MOD_LESSON,
    }
    this.lessonsField = this.elementObj.CreateAutocompleteField(details)
    $(columns).appendTo(this.node);
    
    columns = this.elementObj.CreateTwoColumns();
    this.elementObj.CreateSelectField($(columns).find(".column1"), YesNo, "Published");
    this.published = $(columns).find(".column1 select");
    var details = {parent: $(columns).find(".column2"), id:"validTill", label: "Valid Till"};
    this.validTillField = this.elementObj.CreateDateField(details);
    $(columns).appendTo(this.node);
    
    $("button.js-button-save").unbind().bind("click",  function(){
        this.Save();
    }.bind(this));
    globalThis.course = this;
}
Course.prototype.Edit = function(data)
{
    $(this.title).val(data.title);
    $(this.url).val(data.url);
    this.elementObj.SetHTMLContent(this.description, data.description);
    this.elementObj.SetAutocompleteSelectedItems(this.lessonsField, data.lessons);
    $(this.published).val(data.published);
    this.elementObj.SetDateFieldValue(this.validTillField, data.validTill);
    this.updatedOn = data.updatedOn;
}

Course.prototype.Save = function()
{
    var title = $(this.title).val().trim();
    if(title.length == 0) return Framework.ShowMessage({type:"warning", message:"Enter course title"});

    var url = $(this.url).val().trim();
    if(url.length == 0) return Framework.ShowMessage({type:"warning", message:"Enter valid URL Key"});

    var description = this.elementObj.GetHTMLContent(this.description);
    if(description.length == 0) return Framework.ShowMessage({type:"warning", message:"Enter description"});
    
    var lessons= this.elementObj.GetAutocompleteSelectedItems(this.lessonsField);
    
    var validTill = this.elementObj.GetDateFieldValue(this.validTillField);
    if(validTill == -1) return Framework.ShowMessage({type:"warning", message:"Select a proper Valid Till Date"});

    var data = {
        id: this.id,
        title: title,
        url: url,
        description: description,
        lessonIds: lessons,
        published : $(this.published).val(),
        validTill: validTill,
        updatedOn : this.updatedOn
    };
    Framework.Ajax(MOD_COURSE, COURSE_SAVE, data, function(response){
        if(response.result.state != "ok") return Framework.ShowMessage({type:"error", message:response.result.message});
        this.AfterSave(response.data);
    }.bind(this));
}

Course.prototype.AfterSave = function(data)
{
    this.id = data.id;
    this.updatedOn = data.updatedOn;
    Framework.ShowMessage({type:"success", message:"Course Saved"});
}