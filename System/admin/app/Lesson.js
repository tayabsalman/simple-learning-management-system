function Lesson()
{
    this.node = null;
    this.id = -1;

    this.title = null;
    this.type = null;
    this.url = null;
    this.description = null;
    this.embedVideoCode = null;
    this.attachmentZone = null;
    this.published = null;
    this.updatedOn = null;

    this.elementObj = null;
}

Lesson.prototype.Init = function()
{
    Framework.LoadCSS("Lesson.css");
    this.elementObj = new Elements();
    
    var columns = this.elementObj.CreateOneColumn();
    this.elementObj.CreateInputTextField($(columns).find('.column'), "Title");
    this.title = $(columns).find("input");
    $(columns).appendTo(this.node);

    columns = this.elementObj.CreateTwoColumns();
    this.elementObj.CreateInputTextField($(columns).find(".column1"), "URL Key");
    this.elementObj.CreateSelectField($(columns).find(".column2"), LessonType, "Lesson Type");
    $(columns).appendTo(this.node);
    this.url = $(columns).find(".column1 input");
    this.type = $(columns).find(".column2 select");

    columns = this.elementObj.CreateTwoColumns();
    this.descriptionContainer = $('<div class="js-desciption-container"></div>').appendTo($(columns).find('.column1'));
    this.description = this.elementObj.CreateHTMLField(this.descriptionContainer, "LessonDescription", "Description");
    this.embedVideoContainer = $('<div class="js-embed-video-container"></div>').appendTo($(columns).find('.column2'));
    this.embedVideoCode = this.elementObj.CreateHTMLField(this.embedVideoContainer, "LessonEmbedCode", "Embed Youtube Code");
    $(columns).appendTo(this.node);

    this.attachmentsContainer = $('<div class="js-attachment-container"></div>').appendTo($(columns).find('.column2'));
    if(this.id != -1)
    {
        this.InitializeFileUpload();
    }
    else
        $('<h3 class="js-save-lesson-info">Save Lesson to start uploading Attachments</h3>').appendTo(this.attachmentsContainer);

    columns = this.elementObj.CreateTwoColumns();
    this.elementObj.CreateSelectField($(columns).find(".column1"), YesNo, "Published");
    this.published = $(columns).find(".column1 select");
    $(columns).appendTo(this.node);
    
    $(this.type).unbind().bind("change", this, function(e){
        if($(this).val() == LessonType.Video)
        {
            $(e.data.attachmentsContainer).addClass("hidden");
            $(e.data.embedVideoContainer).removeClass("hidden");
        }
        else
        {
            $(e.data.attachmentsContainer).removeClass("hidden");
            $(e.data.embedVideoContainer).addClass("hidden");
        }
    });
    $(this.type).trigger("change");
    $("button.js-button-save").unbind().bind("click",  function(){
        // this.attachmenZone.processQueue();
        this.Save();
    }.bind(this));
}

Lesson.prototype.InitializeFileUpload = function()
{
    if($(this.attachmentsContainer).find(".js-save-lesson-info").length > 0)
        $(this.attachmentsContainer).find(".js-save-lesson-info").remove();
    var detials = {
        parent: this.attachmentsContainer,
        id: "LessonAttachments",
        label: "Upload Attachments",
        showCount: true
    };
    this.attachmentZone = this.elementObj.CreateFileUploadField(detials);
    this.attachmentsList = $('<ul class="list-group attachment-list"></ul>').appendTo(this.attachmentsContainer);
}

Lesson.prototype.Edit = function(data)
{
    $(this.title).val(data.title);
    $(this.url).val(data.url);
    $(this.type).val(data.type).trigger("change");
    this.elementObj.SetHTMLContent(this.description, data.description);
    if(data.type == LessonType.Video)
        this.elementObj.SetHTMLContent(this.embedVideoCode, data.data);
    else
        this.SetAttachments(data.data);
    
    $(this.type).attr("disabled", "disabled");
    $(this.published).val(data.published);
    this.updatedOn = data.updatedOn;
}

Lesson.prototype.Save = function()
{
    var title = $(this.title).val().trim();
    if(title.length == 0) return Framework.ShowMessage({type:"warning", message:"Enter lesson title"});

    var url = $(this.url).val().trim();
    if(url.length == 0) return Framework.ShowMessage({type:"warning", message:"Enter valid URL Key"});

    var description = this.elementObj.GetHTMLContent(this.description);
    if(description.length == 0) return Framework.ShowMessage({type:"warning", message:"Enter description"});
    
    var embedVideoCode = "";
    if($(this.type).val() == LessonType.Video)
    {
        embedVideoCode = this.elementObj.GetHTMLContent(this.embedVideoCode);
        if(embedVideoCode.length == 0) return Framework.ShowMessage({type:"warning", message:"Enter Video Embed Code"});
    }

    if(this.attachmentZone != null)
        this.uploadFiles = this.attachmentZone.files.length;

    if(this.id != -1 && this.uploadFiles)
        return this.UploadAttachments();
    
    var attachments = this.GetAttachments();

    var data = {
        id: this.id,
        title: title,
        url: url,
        description: description,
        embedVideoCode: embedVideoCode,
        attachments: attachments,
        type : $(this.type).val(),
        published : $(this.published).val(),
        updatedOn : this.updatedOn
    };
    Framework.Ajax(MOD_LESSON, LESSON_SAVE, data, function(response){
        if(response.result.state != "ok") return Framework.ShowMessage({type:"error", message:response.result.message});
        this.AfterSave(response.data);
    }.bind(this));
}

Lesson.prototype.AfterSave = function(data)
{
    this.id = data.id;
    this.updatedOn = data.updatedOn;
    $(this.type).attr("disabled", "disabled");

    if($(this.type).val() == LessonType.Attachment)
    {
        if(this.attachmentZone == null)
            this.InitializeFileUpload();
    }
    Framework.ShowMessage({type:"success", message:"Lesson Saved"});
}

Lesson.prototype.UploadAttachments = function()
{
    var data = {
        itemType: "lesson",
        itemId: this.id,
    };
    Framework.UploadFiles(this.attachmentZone, data, function(response){
        if(response.result.state != "ok")
            return Framework.ShowMessage({type: "error", message: response.result.message});
        
        this.attachmentZone.value = this.attachmentZone.defaultValue;
        $(this.attachmentZone).trigger("change");
        this.SetAttachments(response.data);
        this.Save();
    }.bind(this));
}
Lesson.prototype.SetAttachments = function(attachments)
{
    console.log(attachments);
    for(var i=0; i< attachments.length; i++)
    {
        var parts = attachments[i].split("/");
        var attachmentName = parts[parts.length - 1];
        var attachEntry = $('<li class="list-group-item js-attachment-entry" data-attachment="'+ attachments[i] +'">'+ 
                            '<a  href="'+ attachments[i] +'" target="_blank">' + attachmentName +'</a><span class="text-danger pull-right js-remove-attachment remove-attachment"><i class="fa fa-times"></span></li>');
        $(this.attachmentsList).append(attachEntry);
    }
    $(this.attachmentsList).find(".js-remove-attachment").unbind().bind("click", this, function(e){
        $(this).closest(".js-attachment-entry").remove();
    });
}

Lesson.prototype.GetAttachments = function()
{
    var attachments = [];
    $(this.attachmentsList).find(".js-attachment-entry").each(function(){
        attachments.push($(this).attr("data-attachment"));
    });
    return attachments;
}