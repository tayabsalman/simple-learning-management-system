function Framework(){

}
Framework.controller_URL = "controller_ajax.php";
Framework.appPath = "app/"
Framework.cssPath = "resource/css/";

Framework.Ajax = function(mod, activity, data, callback = false)
{
    data.mod = mod;
    data.activity = activity;

    $.ajax({
        type: "POST",
        data: data,
        complete: function(event){
            var response = event.responseJSON;
            if(typeof(response) == "undefined")
                Framework.ShowMessage({type:"error", message:event});
            else if(callback)
                callback(response);
        },
        error: function(event){
            Framework.ShowMessage({type:"error", message:event.responseJSON});
        },
        url: Framework.controller_URL
    });
}
Framework.UploadFiles = function(fileInput, data = {}, callback = null)
{
    var form_data = new FormData();

   // Read selected files
   var totalfiles = fileInput.files.length;
   for (var index = 0; index < totalfiles; index++) {
      form_data.append("files[]", fileInput.files[index]);
   }

   for(var key in data)
    form_data.append(key, data[key])

   // AJAX request
   $.ajax({
     url: 'file_upload.php', 
     type: 'POST',
     data: form_data,
     dataType: 'json',
     contentType: false,
     processData: false,
     success: function (response) {
         if(callback != null)
            callback(response);
       },
    error: function(err){
        console.log(err);
        Framework.ShowMessage({type:"error", message:"Incountered errors while upload the attachment"});
    }
   });
}
Framework.Init = function()
{
    Framework.LoadJS("App.js", function(){
        Framework.Application = new App();
        Framework.Application.node = Framework.CreateChildNode(document.getElementById("RootApplication"), "Application");
        Framework.Application.Init();
    });
}
Framework.CreateChildNode = function(parent, childId)
{
    var childElement = document.createElement("div");
    childElement.id = childId;
    parent.appendChild(childElement);
    return childElement;
}
Framework.LoadJS = function(filename, callback = false)
{
    if(filename.indexOf("/") < 0)
        filename = Framework.appPath + filename;
    
    var id = "JS_" + filename.replace(/[^a-zA-Z0-9_]+/g, "_");
    if(!$("#"+id).length)
    {
        var scriptTag = document.createElement("script");
        scriptTag.type = "text/javascript";
        scriptTag.id = id;
        scriptTag.async = true;
        scriptTag.onload = function(){
            if(callback)
                callback();
        };
        scriptTag.src = filename;
        document.head.appendChild(scriptTag);
    }
    else if(callback)
        callback();
}

Framework.LoadCSS = function(filename)
{
    var id = filename.replace(/[^a-zA-Z0-9]+/g, "_").replace(".", "_");
    if(!document.getElementById(id))
    {
        linkTag = document.createElement("link");
        linkTag.id = id;
        linkTag.type = "text/css";
        linkTag.rel = "stylesheet";
        linkTag.href = Framework.cssPath + filename;
        document.head.appendChild(linkTag);
    }
}
Framework.ShowMessage = function(data)
{
    Lobibox.notify(data.type, {
        sound: false,
        delay: (data.type == "error")? 5000: 3000,
        // position: (data.type == "error")? 'top center': "bottom right",
        position: "top center",
        msg: data.message
    });
}