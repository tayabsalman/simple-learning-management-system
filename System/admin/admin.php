<?php
$path = '/System/pvt';
set_include_path(get_include_path() . PATH_SEPARATOR . $path);
require_once("system/exceptions.php");
require_once("system/authentication.php");
Authentication::Start(true);
if(!Authentication::IsAuthenticated())
{
  require_once("login.php");
  die();
}

?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Admin Dashboard | Simple Learning Management System</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="theme/img/favicon.ico">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="theme/css/bootstrap.min.css">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="theme/css/font-awesome.min.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="theme/css/owl.carousel.css">
    <link rel="stylesheet" href="theme/css/owl.theme.css">
    <link rel="stylesheet" href="theme/css/owl.transitions.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="theme/css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="theme/css/normalize.css">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="theme/css/meanmenu.min.css">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="theme/css/main.css">
    <!-- educate icon CSS
		============================================ -->
    <link rel="stylesheet" href="theme/css/educate-custon-icon.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="theme/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="theme/css/metisMenu/metisMenu.min.css">
    <link rel="stylesheet" href="theme/css/metisMenu/metisMenu-vertical.css">
    <!-- calendar CSS
		============================================ -->
    <link rel="stylesheet" href="theme/css/calendar/fullcalendar.min.css">
    <link rel="stylesheet" href="theme/css/calendar/fullcalendar.print.min.css">
    <!-- summernote CSS
		============================================ -->
    <link rel="stylesheet" href="theme/css/summernote/summernote.css">
    <!-- datapicker CSS
		============================================ -->
    <link rel="stylesheet" href="theme/css/datapicker/datepicker3.css">
    <!-- chosen CSS
		============================================ -->
    <link rel="stylesheet" href="theme/css/chosen/bootstrap-chosen.css">
    <!-- forms CSS
		============================================ -->
    <link rel="stylesheet" href="theme/css/form/all-type-forms.css">
    <!-- notifications CSS
		============================================ -->
    <link rel="stylesheet" href="theme/css/notifications/Lobibox.min.css">
    <link rel="stylesheet" href="theme/css/notifications/notifications.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="theme/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="theme/css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="theme/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!-- Start Left menu area -->
    <div class="left-sidebar-pro">
        <nav id="sidebar" class="">
            <div class="sidebar-header">
                <a href="admin.php"><img class="main-logo" src="theme/img/logo/logo.png" alt="" /></a>
                <strong><a href="admin.php"><img src="theme/img/logo/logosn.png" alt="" /></a></strong>
            </div>
            <div class="left-custom-menu-adp-wrap comment-scrollbar">
                <nav class="sidebar-nav left-sidebar-menu-pro">
                    <ul class="metismenu" id="menu1">
                        <li class="js-sidebar-element">
                            <a class="has-arrow" href="#" aria-expanded="false"><span class="educate-icon educate-library icon-wrap"></span> <span class="mini-click-non">Lessons</span></a>
                            <ul class="submenu-angle" aria-expanded="false">
                                <li><a id="all-lessons" title="All Lessons" href="#all-lessons"><span class="mini-sub-pro">All Lessons</span></a></li>
                                <!-- <li><a id="js-add-lesson" title="Add Lesson" href="#"><span class="mini-sub-pro">Add Lesson</span></a></li> -->
                            </ul>
                        </li>
                        <li class="js-sidebar-element">
                            <a class="has-arrow" href="#" aria-expanded="false"><span class="educate-icon educate-course icon-wrap"></span> <span class="mini-click-non">Courses</span></a>
                            <ul class="submenu-angle" aria-expanded="false">
                                <li><a id="all-courses" title="All Courses" href="#all-courses"><span class="mini-sub-pro">All Courses</span></a></li>
                                <!-- <li><a id="js-add-course" title="Add Course" href="#"><span class="mini-sub-pro">Add Course</span></a></li> -->
                            </ul>
                        </li>
                        <li class="js-sidebar-element">
                            <a class="has-arrow" href="#" aria-expanded="false"><span class="educate-icon educate-student icon-wrap"></span> <span class="mini-click-non">Manage Users</span></a>
                            <ul class="submenu-angle" aria-expanded="false">
                                <li><a id="all-users" title="All Users" href="#all-users"><span class="mini-sub-pro">All Users</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </nav>
    </div>
    <!-- End Left menu area -->
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="logo-pro">
                        <a href="/"><img class="main-logo" src="theme/img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-advance-area">
            <div class="header-top-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header-top-wraper">
                                <div class="row">
                                    <div class="col-lg-1 col-md-0 col-sm-1 col-xs-12">
                                        <div class="menu-switcher-pro">
                                            <button type="button" id="sidebarCollapse" class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
													<i class="educate-icon educate-nav"></i>
												</button>
                                        </div>
                                    </div>
                                    <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12">
                                        <div class="header-right-info">
                                            <ul class="nav navbar-nav mai-top-nav header-right-menu">
                                                <li class="nav-item">
                                                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                                                        <!-- <img src="theme/img/product/pro4.jpg" alt=""> -->
                                                        <span class="admin-name js-admin-name">Prof.Anderson</span>
                                                        <i class="fa fa-angle-down edu-icon edu-down-arrow"></i>
                                                    </a>
                                                    <ul role="menu" class="dropdown-header-top author-log dropdown-menu animated zoomIn">
                                                        <li><a href="#" class="js-logout"><span class="edu-icon edu-locked author-log-ic"></span>Log Out</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="RootApplication" class="root-application">
        </div>
    </div>

    <!-- jquery
		============================================ -->
    <script src="theme/js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="theme/js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="theme/js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="theme/js/jquery-price-slider.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="theme/js/jquery.meanmenu.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="theme/js/owl.carousel.min.js"></script>
    <!-- sticky JS
		============================================ -->
    <script src="theme/js/jquery.sticky.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="theme/js/jquery.scrollUp.min.js"></script>
    <!-- counterup JS
		============================================ -->
    <script src="theme/js/counterup/jquery.counterup.min.js"></script>
    <script src="theme/js/counterup/waypoints.min.js"></script>
    <script src="theme/js/counterup/counterup-active.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="theme/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="theme/js/scrollbar/mCustomScrollbar-active.js"></script>
    <!-- metisMenu JS
		============================================ -->
    <script src="theme/js/metisMenu/metisMenu.min.js"></script>
    <script src="theme/js/metisMenu/metisMenu-active.js"></script>
    <!-- calendar JS
		============================================ -->
    <script src="theme/js/calendar/moment.min.js"></script>
    <script src="theme/js/calendar/fullcalendar.min.js"></script>
    <script src="theme/js/calendar/fullcalendar-active.js"></script>
     <!-- summernote JS
		============================================ -->
    <script src="theme/js/summernote/summernote.min.js"></script>
    <!-- datapicker JS
		============================================ -->
    <script src="theme/js/datapicker/bootstrap-datepicker.js"></script>
    <!-- chosen JS
		============================================ -->
    <script src="theme/js/chosen/chosen.jquery.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="theme/js/plugins.js"></script>
    <!-- notification JS
		============================================ -->
    <script src="theme/js/notifications/Lobibox.js"></script>
    <!-- main JS
		============================================ -->
    <script src="theme/js/main.js"></script>
    <!-- tawk chat JS
		============================================ -->
    <script src="framework/framework.js"></script>
    <script>
      Framework.Init();
    </script>
</body>

</html>