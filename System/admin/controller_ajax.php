<?php
if(!isset($_REQUEST['mod']) || !isset($_REQUEST['activity']))
	die("Internal Error");
// Setup Include Path
$path = '/System/pvt';
set_include_path(get_include_path() . PATH_SEPARATOR . $path);
require_once("system/exceptions.php");
require_once("system/authentication.php");

$result = array(
    "data"=> NULL,
    "result"=> array(
        "state"=>"",
        "meta"=>""
    )
);
try
{
    $module = $_REQUEST['mod'];
	$activity = $_REQUEST['activity'];
    require_once("controller/$module.php");
    
    Authentication::Validate(true);
    
    $controllerClass = $module."Controller";
	$controllerActivity = $activity."Ack";
	
	$controller = new $controllerClass();
	$data = $controller->$controllerActivity();
	
	$result["data"] = $data;
	$result["result"]["state"] ="ok";
}
catch(DBException $e)
{
    $result["result"]["state"] = "database Error";
    $result["result"]["message"] = $e->getMessage();
}
catch(InvalidDataException $e)
{
    $result["result"]["state"] = "invalid Data";
    $result["result"]["message"] = $e->getMessage();
}
catch(AuthException $e)
{
    $result["result"]["state"] = "un-authorized";
    $result["result"]["message"] = $e->getMessage();
}
catch(Exception $e)
{
    $result["result"]["state"] = "error";
    $result["result"]["message"] = $e->getMessage();
}
header("Content-Type: application/json");
echo json_encode($result);
$result = NULL;
?>
