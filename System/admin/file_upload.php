<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);

if(!isset($_REQUEST['itemType']) || !isset($_REQUEST['itemId']))
	die("Internal Error");
// Setup Include Path
$path = '/System/pvt';
set_include_path(get_include_path() . PATH_SEPARATOR . $path);
require_once("system/exceptions.php");
require_once("system/authentication.php");

$result = array(
    "data"=> NULL,
    "result"=> array(
        "state"=>"",
        "meta"=>""
    )
);

try
{
    Authentication::Start(true);
    if(!Authentication::IsAuthenticated())
        throw new AuthException("Authentication Timout, please refresh the site and try again");

    $target_dir = "/System/uploads";
    // if(!is_dir($target_dir)) mkdir($target_dir);
    $itemId = $_REQUEST["itemId"];
    $target_dir = $target_dir . "/". $_REQUEST['itemType']. "-" .$itemId;
    if(!is_dir($target_dir)) mkdir($target_dir);
    
    $valid_ext = array("jpeg","jpg",  "pdf", "doc", "docx", "txt", "pptx");
    $files_arr = array();
    $fileCount = count($_FILES['files']['name']);

    if($fileCount > 2)
        throw new InvalidDataException("Cannot upload more than two files at a time");
    
    for($index = 0;$index < $fileCount;$index++)
    {
        $filename = $_FILES['files']['name'][$index];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(in_array($ext, $valid_ext))
        {
            $path = $target_dir. "/" .$filename;
            if(move_uploaded_file($_FILES['files']['tmp_name'][$index],$path))
            {
                $files_arr[] = str_replace("/System/", "", $path);
            }
        }
        else
            throw new InvalidDataException("Upload file only with extensions: ". implode(", ", $valid_ext));
    }

	$result["data"] = $files_arr;
	$result["result"]["state"] ="ok";
}
catch(AuthException $e)
{
    $result["result"]["state"] = "un-authorized";
    $result["result"]["message"] = $e->getMessage();
}
catch(InvalidDataException $e)
{
    $result["result"]["state"] = "in-valid data";
    $result["result"]["message"] = $e->getMessage();
}
catch(Exception $e)
{
    $result["result"]["state"] = "error";
    $result["result"]["message"] = $e->getMessage();
}
header("Content-Type: application/json");
echo json_encode($result);
$result = NULL;
?>
