function Editor()
{
    if($('#Editor').length > 0){
		$('#Editor').remove();
    }
    this.editor = null;
	this.editorFrame = null;
	this.closeCallBack = null;
}

Editor.prototype.Init = function()
{
    Framework.LoadCSS("Editor.css");
    this.editor = Framework.CreateChildNode($("#RootApplication")[0], "Editor");
    $(this.editor).addClass("hidden");

    var header = $('<div class="container editorHead"><h4 class="editor-title js-editor-title"></h4><span class="pull-right js-editor-close editor-close text-danger"><i class="fa fa-times"></span></div>');
    $(this.editor).append(header);
    
    this.editorFrame = $('<div id="EditorFrame" class="container"></div>').appendTo(this.editor);

    var footer = $('<div class="editorFooter" ><div class="footer-buttons pull-right">'+
                    '<div class="login-btn-inner form-bc-ele"><button class="btn btn-sm btn-primary login-submit-cs js-button-save">Save Changes</button></div>'+
                    '</div>');
    $(this.editor).append(footer);

    $(this.editor).find(".js-editor-close").unbind().bind("click", function(){
        this.Close();
    }.bind(this))
}

Editor.prototype.Open = function(title)
{
    $(this.editor).find(".js-editor-title").html(title);
    $(this.editor).removeClass("hidden");
    $("body").css("overflow", "hidden");
    $(this.editor).css("overflow", "auto");
}
Editor.prototype.Close = function()
{
    $(this.editorFrame).empty();
    $(this.editor).addClass("hidden");
    $("body").css("overflow", "auto");
}