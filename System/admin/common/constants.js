//Controllers
MOD_LESSON = "lesson";
MOD_COURSE = "course"
MOD_USER = "user";

//Activities
LESSON_SAVE = "add";
COURSE_SAVE = "add";
LESSON_GET_ATTACHMENTS = "getattachments";
USER_LOGOUT = "logout";
USER_SELFAUTH = "selfauth";

const LessonType = {
    Video : 0,
    Attachment : 1,
    Str : [
        "VIDEO",
        "ATTACHMENT"
    ]
};

const YesNo = {
    No : 0,
    Yes : 1,
    Str : [
        "NO",
        "YES"
    ]
}
