function Elements()
{
    Framework.LoadCSS("Elements.css");
}

Elements.prototype.CreateButton = function(btnText, btnClass)
{
    return $('<button class="btn ' + btnClass + '">' + btnText + '</button>');
}

Elements.prototype.CreateListStructure = function()
{
    return $('<div class="product-status mg-b-15">' +
                '<div class="container-fluid">' +
                    '<div class="row">' +
                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">' +
                            '<div class="product-status-wrap">' +
                            '<h4><span class="js-list-title">Library List</span>(<span class="js-total-count">0</span>)</h4>' +
                            '<h6>Showing &nbsp<span class="js-current-count">Library List</span>/<span class="js-total-count"></span>&nbsp items</h6>' +
                            '<div class="asset-inner"><table><tbody></tbody></table></div>' +
                            '<div class="custom-pagination"><ul class="pagination js-pagination"></ul></div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>');
}

Elements.prototype.CreateTwoColumns = function()
{
    var row = $('<div class="row elementBlock"></div>');
    $('<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 column1"></div>').appendTo(row);
    $('<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 column2"></div>').appendTo(row);
    return row;
}
Elements.prototype.CreateOneColumn = function()
{
    var row = $('<div class="row elementBlock"></div>');
    $('<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 column"></div>').appendTo(row);
    return row;
}
Elements.prototype.CreateInputTextField = function(parent, labelText = null)
{
    var formGroup = $('<div class="form-group"></div>');
    if(labelText != null)
        $(formGroup).append($('<label>' + labelText + '<\label>'));
    $('<input class="form-control" type="text" placeholder="'+ labelText +'">').appendTo(formGroup);
    $(parent).append(formGroup);
    return formGroup;
}
Elements.prototype.CreateSelectField = function(parent, selectData, labelText = null, selected = null)
{
    var formSelect = $('<div class="form-select-list"></div>');
    if(labelText != null)
        $('<label>'+ labelText + '</label>').appendTo(formSelect);
    var selectTag = $('<select class="form-control custom-select-value"></select>').appendTo(formSelect);
    for(var i=0; i< selectData.Str.length; i++)
    {
        optionSelected = (i == selected)? 'selected="selected"' : '';
        $('<option value="'+ i +'">'+ selectData.Str[i] +'</option>').appendTo(selectTag);
    }
    return formSelect.appendTo(parent);
}
Elements.prototype.CreateHTMLField = function(parent, id, labelText = null)
{
    if(labelText != null)
        $('<label>'+ labelText +'</label>').appendTo(parent);
    
    var element = $('<div id="'+ id +'"></div>').appendTo(parent);
    $(element).summernote({
        height:200
    });
    $(element).summernote("code", "");
    return element;
}
Elements.prototype.GetHTMLContent = function(HTMLField)
{
    return $(HTMLField).summernote("code");
}
Elements.prototype.SetHTMLContent = function(HTMLField, content)
{
    return $(HTMLField).summernote("code", content);
}
Elements.prototype.CreateFileUploadField = function(data)
{
    if(data.label)
        $('<label>'+ data.label +'</label>').appendTo(data.parent);
    var wrapper = $('<div class="file-upload-inner-right file-upload-inner ts-forms"></div>').appendTo(data.parent);
    var fileButton = $('<div class="file-button"><span>SELECT</span></div>').appendTo(wrapper);
    $('<input type="file" id="'+ data.id +'" multiple="">').appendTo(fileButton);
    $('<label class="hidden js-count"></label>').appendTo(wrapper);
    if(data.showCount)
    {
        $('#'+data.id).on("change", this, function(){
            var count = $(this)[0].files.length;
            if(count)
            {
                var messageCount = count + ((count > 1)?" Files": " File") + " will be uploaded";
                $(this).closest(".file-upload-inner").find("label.js-count").html(messageCount);
                $(this).closest(".file-upload-inner").find("label.js-count").removeClass("hidden");
            }
            else
                $(this).closest(".file-upload-inner").find("label.js-count").addClass("hidden");
        });
    }
    return document.getElementById(data.id);
}
Elements.prototype.CreateDateField = function(data)
{
    var dateWrapper = $('<div class="form-group" id="'+ data.id +'-date"></div>').appendTo(data.parent);
    if(typeof data.label != "undefined")
        $('<label>'+ data.label +'</label>').appendTo(dateWrapper);
    $('<div class="input-group date">' + 
        '<span class="input-group-addon"><i class="fa fa-calendar"></i></span>' + 
        '<input type="text" class="form-control" value=""></input>' + 
      '</div>').appendTo(dateWrapper);
    
    $(dateWrapper).find(".input-group.date").datepicker({
        todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
        autoclose: true
    });
    this.SetDateFieldValue(dateWrapper);
    return dateWrapper;
}
Elements.prototype.GetDateFieldValue = function(dateField)
{
    var dateObject = $(dateField).find(".input-group.date").datepicker("getDate");
    var date = String(dateObject.getDate());
    var month = String(dateObject.getMonth() + 1);
    var year = String(dateObject.getFullYear());
    
    if(date < 10) date = '0' + date;
    if(month < 10) month = '0' + month;

    if(date == "NaN" || month == "NaN" || year == "NaN")
        return -1;
    return [year, month, date].join("-");
}
Elements.prototype.SetDateFieldValue = function(dateField, value=null)
{
    if(value != null && value.length)
        $(dateField).find(".input-group.date").datepicker("setDate", new Date(value));
    else
        $(dateField).find(".input-group.date").datepicker("setDate", new Date());
}
Elements.prototype.CreateAutocompleteField = function(data)
{
    //Creating Chosen Field
    var wrapper = $('<div id="'+ data.id +'-wrapper" class="autocomplete-element"></div>').appendTo(data.parent);
    $('<div class="chosen-select-multiple">'+
        '<label>'+ data.label +'</label>' + 
        '<select data-placeholder="'+ data.placeholder +'" class="chosen-select" multiple="" tabindex="-1"></select>' + 
      '</div>').appendTo(wrapper);
    $(wrapper).find(".chosen-select").chosen({width: "100%"});

    //Populating data dynamically
    $(wrapper).find(".search-field input").on("keyup", function(){
        var searchStr = $(this.wrapper).find(".search-field input").val();
        console.log({searchStr});
        Framework.Ajax(this.data.controller, "list", {searchStr: searchStr}, function(response){
            if(response.result.state != "ok") return Framework.ShowMessage({type:"error", message:response.result.message});

            $(this).find("select option").not(":selected").remove();
            response.data.forEach(element => {
                if($(this).find("select option[value='"+element.id+"']").length == 0)
                    $(this).find("select").prepend('<option value="'+ element.id +'">'+ element.title +'</option>');
            });
            $(this).find(".chosen-select").trigger("chosen:updated");
            $(this).find(".search-field input").val(searchStr);
        }.bind(wrapper))
    }.bind({data: data, wrapper: wrapper}));

    return wrapper;
}
Elements.prototype.GetAutocompleteSelectedItems = function(autocompleteElement)
{
    var itemIds = $(autocompleteElement).find(".chosen-select").val();
    return (itemIds == null)? [] : itemIds;
}
Elements.prototype.SetAutocompleteSelectedItems = function(autocompleteElement, items)
{
    var itemIds = [];
    items.forEach(element => {
        $(autocompleteElement).find(".chosen-select").append('<option value="'+ element.id +'">'+ element.title +'</option>');
        itemIds.push(element.id);
    });
    $(autocompleteElement).find(".chosen-select").val(itemIds).trigger("chosen:updated");
}