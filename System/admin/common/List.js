var listConfig = {
    Lesson:[
        {name: "Id", id: "id"},
        {name: "Title", id:"title"},
        {name: "Published", id:"published"},
        {name: "Type", id:"type"},
        {name: "Created", id:"createdOn"},
        {name: "Actions", id:"action"}
    ],
    Course:[
        {name: "Id", id: "id"},
        {name: "Title", id:"title"},
        {name: "Published", id:"published"},
        {name: "Total Lessons", id:"lessonCount"},
        {name: "Valid Till", id:"validTill"},
        {name: "Created", id:"createdOn"},
        {name: "Actions", id:"action"}
    ],
    User:[
        {name: "Id", id: "id"},
        {name: "Name", id:"name"},
        {name: "Email ID", id:"email"},
        {name: "Role", id:"role"},
        {name: "Registered On", id:"createdOn"},
    ],
};

function List()
{
    this.node = null;
    this.controller = null;
    this.editor = null;
    this.title = "";
    this.pluralTitle = "";

    this.editorObj = null;
    this.editorFrame = null;

    this.refreshBtn = null;
    this.listTable = null;
    this.itemsFrom = 0;
    this.itemsPerPage = 2;
    this.searchStr = "";
    this.columnIds = [];
}

List.prototype.Init = function()
{
    var elementsObj = new Elements();
    var actionPanel = $('<div class="breadcome-area"><div class="container-fluid">' +
                           '<div class="row"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="breadcome-list">' +
                           '<div class="row"><div class="col-lg-6 col-md-6 col-sm-8 col-xs-12"><div class="js-search-div"></div></div>' + 
                           '<div class="col-lg-6 col-md-6 col-sm-4 col-xs-12"><div class="login-btn-inner"><div class="cancel-wp pull-right form-bc-ele js-listbuttons-div"></div></div></div></div>' +
                           '</div></div></div></div></div>').appendTo(this.node);
    
    var searchDiv = $(actionPanel).find(".js-search-div");

    $(searchDiv).append('<div class="breadcome-heading">' +
                                '<form role="search" class="sr-input-func">' +
                                    '<input type="text" placeholder="Search..." class="search-int form-control js-search-field">' +
                                    '<a href="#" class="js-search-icon"><i class="fa fa-search"></i></a>' +
                                '</form>' +
                            '</div>');
    
    this.searchField = $(searchDiv).find("input.js-search-field");
    $(this.searchField).on("keydown", this, function(e){
        if(e.which == 13 || e.keyCode == 13)
            e.data.RefreshList();
    });
    $(searchDiv).find(".js-search-icon").unbind().bind("click", this, function(e){
        e.data.RefreshList();
    });

    var listButtonsDiv = $(actionPanel).find(".js-listbuttons-div");
    this.refreshBtn = elementsObj.CreateButton("Refresh", "btn-orange").appendTo(listButtonsDiv);
    var addNewBtn = elementsObj.CreateButton("Add New " + this.title,  "btn-sm btn-primary login-submit-cs").appendTo(listButtonsDiv);

    $(this.refreshBtn).unbind().bind("click", function(e){
        $(this.searchField).val("");
        this.RefreshList();
    }.bind(this));
    
    if(this.controller != MOD_USER)
    {
        $(addNewBtn).unbind().bind("click", function(e){
            this.AddNewItem();
        }.bind(this));
    }
    else
        $(addNewBtn).addClass("hidden");

    var listStructure = elementsObj.CreateListStructure().appendTo(this.node);
    $(listStructure).find(".js-list-title").html("Total " + this.pluralTitle);
    this.listTable = $(listStructure).find("tbody");
    var tableHead = $('<tr class="js-table-head"></tr>').appendTo(this.listTable);
    listConfig[this.editor].forEach(column => {
        $('<th>' + column.name +'</th>').appendTo(tableHead);
        this.columnIds.push(column.id);
    });
    
    this.pagination = $(listStructure).find(".js-pagination");
    this.RefreshList();
}

List.prototype.RefreshList = function()
{
    this.itemsFrom = 0;
    this.searchStr = $(this.searchField).val().trim();
    this.LoadList();
}

List.prototype.LoadList = function()
{
    this.UpdatePagination();
    this.CreateList();
}

List.prototype.UpdatePagination = function()
{
    Framework.Ajax(this.controller, "count", {searchStr: this.searchStr}, function(response){
        if(response.result.state == "ok")
        {
            $(this.pagination).addClass("hidden");
            $(this.pagination).empty();

            var currentPage = Math.ceil(this.itemsFrom/this.itemsPerPage) + 1;
            $(this.node).find(".js-total-count").html(response.data);
            $(this.node).find(".js-current-count").html((currentPage * this.itemsPerPage <= response.data)? currentPage * this.itemsPerPage: response.data);

            var totalPages = lastPage = Math.ceil(response.data/this.itemsPerPage);
            var PaginationWindow = 5;
            if(totalPages > 1)
            {
                var paginationHtml = "";
                var disabledClass = (currentPage == 1)?"disabled": "";
                if(totalPages > PaginationWindow)
                    paginationHtml += '<li class="page-item js-controls '+ disabledClass +'" data-pageNumber="1"><a href="#" class="page-link">First</a></li>';
                paginationHtml += '<li class="page-item js-controls '+ disabledClass +'" data-pageNumber="' + (currentPage - 1) + '"><a href="#" class="page-link"><i class="fa fa-angle-double-left"></i></a></li>';
                paginationHtml
                if(totalPages > PaginationWindow)
                {
                    var middlePage = (currentPage >= totalPages-2) ? totalPages-2 : (currentPage <=3) ? 3 : currentPage;
                    for(var i=-2; i <=2; i++)
                        paginationHtml += '<li class="page-item" data-pageNumber="'+ (middlePage + i) +'"><a href="#" class="page-link">'+ (middlePage + i) +'</a></li>';
                }
                else
                {
                    for(var i=1; i<= totalPages; i++)
                        paginationHtml += '<li class="page-item" data-pageNumber="'+ i +'"><a href="#" class="page-link">'+ i +'</a></li>';
                }
                disabledClass = (currentPage == lastPage)? "disabled": "";
                paginationHtml += '<li class="page-item js-controls '+ disabledClass +'" data-pageNumber="'+ (currentPage + 1) +'"><a href="#" class="page-link"><i class="fa fa-angle-double-right"></i></a></li>';
                if(totalPages > PaginationWindow)
                    paginationHtml += '<li class="page-item js-controls '+ disabledClass +'" data-pageNumber="'+ lastPage +'"><a href="#" class="page-link">Last</a></li>';
                $(this.pagination).html(paginationHtml);
                $(this.pagination).removeClass("hidden");

                $(this.pagination).find('.page-item:not(.js-controls)[data-pageNumber="'+ currentPage +'"]').addClass("active");

                $(this.pagination).find('.page-item:not(.disabled)').unbind().bind("click", this, function(e){
                    var itemsFrom = parseInt($(this).attr("data-pageNumber")) * e.data.itemsPerPage - e.data.itemsPerPage;
                    if(itemsFrom == e.data.itemsFrom) return;
                    e.data.itemsFrom = itemsFrom;
                    e.data.LoadList();
                });
                $('.pagination .disabled *, .pagination .active *').unbind().bind("click", function(e){
                    e.preventDefault();
                });
            }
        }

    }.bind(this));
}
List.prototype.CreateList = function()
{
    var data = {
        from : this.itemsFrom,
        count : this.itemsPerPage,
        searchStr : this.searchStr
    };
    Framework.Ajax(this.controller, "list", data, function(response){
        if(response.result.state == "ok")
        {
            $(this.listTable).find('tr:not(.js-table-head)').remove();
            if(response.data.length > 0)
            {
                for(var i=0; i < response.data.length; i++)
                    this.AddItemToList(response.data[i]);
            
                $(this.node).find(".js-edit-item").unbind().bind("click", this, function(e){
                    e.data.EditItem($(this).closest("tr.js-list-item"));
                });
            }
        }
        else
            Framework.ShowMessage({type:"error", message:response.result.message});
    }.bind(this));
}

List.prototype.AddNewItem = function(itemDom)
{
    if(this.editorFrame == null)
    {
        this.editorObj = new Editor();
        this.editorObj.Init();
        this.editorFrame = this.editorObj.editorFrame;
    }

    Framework.LoadJS(this.editor + ".js", function(){
        itemObj = new window[this.editor]();
        itemObj.node = this.editorFrame;
        itemObj.Init();
        this.editorObj.Open("Create new " + this.title);
    }.bind(this))
}

List.prototype.EditItem = function(itemDom)
{
    if(this.editorFrame == null)
    {
        this.editorObj = new Editor();
        this.editorObj.Init();
        this.editorFrame = this.editorObj.editorFrame;
    }

    Framework.LoadJS(this.editor + ".js", function(){
        this.editorObj.Open($(itemDom).attr("data-title"));
        itemObj = new window[this.editor]();
        itemObj.node = this.editorFrame;
        itemObj.id = $(itemDom).attr("data-id");
        Framework.Ajax(this.controller, "edit", {id: itemObj.id}, function(response){
            if(response.result.state != "ok")
                return Framework.ShowMessage({type:"error", message:response.result.message});
            itemObj.Init();
            itemObj.Edit(response.data);
        })
    }.bind(this))
}

List.prototype.AddItemToList = function(itemData)
{
    console.log("Add item to list " + itemData.id);
    var itemEntry = $('<tr class="js-list-item" data-id="'+ itemData.id + '"></tr>');

    if(this.columnIds.indexOf("id") !== -1)
        $('<td>'+ this.title.toLowerCase() + "-000" +itemData.id +'</td>').appendTo(itemEntry);
    if(this.columnIds.indexOf("title") !== -1)
    {
        var itemTitle = (itemData.title.length > 40)? itemData.title.substring(0, 40 ) + "...": itemData.title;
        $('<td>'+ itemTitle +'</td>').appendTo(itemEntry);
        $(itemEntry).attr("data-title", itemTitle);
    }
    if(this.columnIds.indexOf("published") !== -1)
        $('<td>'+ itemData.published +'</td>').appendTo(itemEntry);
    if(this.columnIds.indexOf("type") !== -1)
        $('<td>'+ itemData.type +'</td>').appendTo(itemEntry);
    if(this.columnIds.indexOf("lessonCount") !== -1)
        $('<td>'+ itemData.lessonCount +'</td>').appendTo(itemEntry);
    if(this.columnIds.indexOf("validTill") !== -1)
        $('<td>'+ itemData.validTill +'</td>').appendTo(itemEntry);
    if(this.columnIds.indexOf("name") !== -1)
        $('<td>'+ itemData.name +'</td>').appendTo(itemEntry);
    if(this.columnIds.indexOf("email") !== -1)
        $('<td>'+ itemData.email +'</td>').appendTo(itemEntry);
    if(this.columnIds.indexOf("role") !== -1)
        $('<td>'+ itemData.role +'</td>').appendTo(itemEntry);
    if(this.columnIds.indexOf("createdOn") !== -1)
        $('<td>'+ itemData.createdOn +'</td>').appendTo(itemEntry);
    if(this.columnIds.indexOf("action") !== -1)
        $('<td>' + 
            '<button data-toggle="tooltip" title="Edit" class="js-edit-item pd-setting-ed" data-original-title="Edit">' +
                '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>' +
            '</button>' + 
          '</td>').appendTo(itemEntry);
    
    $(this.listTable).append(itemEntry);
}
