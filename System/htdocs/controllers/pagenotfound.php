<?php
require_once("libraries/helper.php");
Helper::PermitFurther();

require_once("libraries/output.php");
require_once("libraries/urlmgnt.php");
global $g_data;
$urlMgnt = URLMgnt::Object();
$g_data["data"] = array(
    "baseUrl" => $urlMgnt->baseUrl,
    "primaryUrl" => $urlMgnt->primaryUrl
);

$output = Output::Object();
$output->SetView("pagenotfound.html");