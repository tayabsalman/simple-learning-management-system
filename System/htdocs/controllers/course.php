<?php
require_once("libraries/helper.php");
Helper::PermitFurther();

require_once("libraries/urlmgnt.php");
require_once("libraries/output.php");
require_once("entity/course.php");

global $g_data;

$urlmgnt = URLMgnt::Object();
$courseEntity = new courseEntity();
$courseEntity->id = $urlmgnt->itemId;
$g_data["data"] = $courseEntity->GetPage();
$courseEntity->Close();

$output = Output::Object();
$output->SetView("course.html");