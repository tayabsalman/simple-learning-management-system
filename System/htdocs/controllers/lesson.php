<?php
require_once("libraries/helper.php");
Helper::PermitFurther();

require_once("libraries/urlmgnt.php");
require_once("libraries/output.php");
require_once("entity/lesson.php");

global $g_data;

$urlmgnt = URLMgnt::Object();
$lessonEntity = new lessonEntity();
$lessonEntity->id = $urlmgnt->itemId;
$g_data["data"] = $lessonEntity->GetPage();
$lessonEntity->Close();

$output = Output::Object();
$output->SetView("lesson.html");