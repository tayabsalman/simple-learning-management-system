<?php
require_once("libraries/helper.php");
Helper::PermitFurther();
require_once("libraries/output.php");
require_once("system/authentication.php");
require_once("entity/course.php");

global $g_data;

$g_data["userData"] = array();
$g_data["userData"]["name"] = Authentication::GetAuthData()["name"];

$courseEntity = new courseEntity();
$g_data["data"]["courses"] = $courseEntity->GetCourseList();
$courseEntity->Close();

$output = Output::Object();
$output->SetView("home.html");