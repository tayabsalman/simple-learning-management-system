<?php
require_once("libraries/helper.php");
Helper::PermitFurther();

require_once("libraries/output.php");

$view = "login.html";
if(Helper::GetParam("registeruser"))
    $view = "registration.html";

$output = Output::Object();
$output->SetView($view);