$(document).ready(function(){
    $(".js-logout-btn").unbind().bind("click", function(){
        Framework.Ajax("user", "logout", {}, function(response){
            if(response.result.state == "ok")
                window.location.reload();
        });
    });
    $(".js-access-btn").unbind().bind("click", this, function(){
        window.location = $(this).data("url");
    });
});