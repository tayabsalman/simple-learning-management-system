function Framework(){

}
Framework.controller_URL = "controller_ajax.php";
Framework.Ajax = function(mod, activity, data, callback = false)
{
    data.mod = mod;
    data.activity = activity;

    $.ajax({
        type: "POST",
        data: data,
        complete: function(event){
            var response = event.responseJSON;
            if(typeof(response) == "undefined")
                alert(event);
            else if(callback)
                callback(response);
        },
        error: function(event){
            alert(event.responseJSON)
        },
        url: Framework.controller_URL
    });
}