<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);
$includePath = "/System/pvt";
set_include_path(get_include_path() . PATH_SEPARATOR . $includePath);
require_once("template-engine/vendor/autoload.php");

$g_data = array();
require_once("libraries/urlmgnt.php");
require_once("libraries/helper.php");
require_once("libraries/output.php");

require_once("system/authentication.php");
Authentication::Start();
try
{
    $urlMgnt = URLMgnt::Object();
    $output = Output::Object();

    if(!Authentication::IsAuthenticated())
        $urlMgnt->SetRoute("unauthenticated");
    
    $urlMgnt->Route();

    $output->Display();
}
catch(Exception $e)
{
    throw $e;
}
