<?php
require_once("libraries/helper.php");
Helper::PermitFurther();
require_once("/System/pvt/template-engine/vendor/autoload.php");
class Output
{
    private static $output = NULL;
    private $view = "";
    
    private function __construct()
    {

    }

    public static function Object()
    {
        if(self::$output == NULL)
            self::$output = new Output();
        return self::$output;
    }

    public function Display()
    {
        $loader = new \Twig\Loader\FilesystemLoader('/System/htdocs/views');
        $twig = new \Twig\Environment($loader);
        global $g_data;
        echo $twig->render($this->view, $g_data);
    }

    public function SetView($viewName)
    {
        $this->view = "views/" . $viewName;
    }
}