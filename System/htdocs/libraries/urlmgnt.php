<?php
require_once("libraries/helper.php");
Helper::PermitFurther();

require_once("entity/urlmgnt.php");

class URLMgnt
{
    public $baseUrl = "";
    public $primaryUrl = "";
    private $presetUrl = array('/'=>"home", "/hello"=>"hello");
    private $route = "";
    public $itemId = -1;
    private static $urlMgnt = NULL;
    private function __construct()
    {
        $this->baseUrl = urldecode(strtolower(explode("?", $_SERVER["REQUEST_URI"], 2)[0]));
        if($this->baseUrl != "/")
            $this->baseUrl = rtrim($this->baseUrl, "/");
        $urlParts = explode("/", $this->baseUrl);
        $this->primaryUrl = array_pop($urlParts);
    }

    public static function Object()
    {
        if(self::$urlMgnt == NULL)
            self::$urlMgnt = new URLMgnt();
        return self::$urlMgnt;
    }

    public function Route()
    {
        if($this->route == "")
        {
            if(isset($this->presetUrl[$this->baseUrl]))
                $this->route = $this->presetUrl[$this->baseUrl];
            else
            {
                $urlMgntEntity = new urlMgntEntity();
                $urlMgntEntity->urlKey = $this->primaryUrl;
                $action = $urlMgntEntity->GetAction();
                $urlMgntEntity->Close();
                if($action != -1)
                {
                    $actionParts = explode("&", $action);
                    $this->route = $actionParts[0];
                    $this->itemId = explode("=", $actionParts[1])[1];
                }
            }
        }
        if($this->route == "")
            $this->route = 'pagenotfound';
        
        require_once("/System/htdocs/controllers/". $this->route . ".php");
    }

    public function SetRoute($route)
    {
        $this->route = $route;
    }
}