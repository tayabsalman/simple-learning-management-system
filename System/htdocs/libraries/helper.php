<?php
Helper::PermitFurther();
class Helper
{
    public static function PermitFurther()
    {
        if(isset($GLOBALS["g_data"]))
            return;
        else
            throw new Exception("Un-authorized");
    }

    public static function GetParam($param)
    {
        if(isset($_GET[$param]))
            return $_GET[$param];
        return null;
    }
}