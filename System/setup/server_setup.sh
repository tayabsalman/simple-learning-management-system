#!/bin/sh

#App server
sudo apt install -y nginx
sudo apt install -y php-fpm
sudo apt install -y php-mysql

#DB server
sudo apt install -y mysql-server
