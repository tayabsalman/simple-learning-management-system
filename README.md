# Simple Learning Management System
A simple learning management system in which admin/teacher can  create and publish courses for the users/students.
A course contains lessons which will be either a video lesson or attachments files for example PDF files. Users will be able to access the courses and lesson after logging into their portal.

## Deployment environment
The following information indicates the installation/setup of this project into Linux(Ubuntu) environment.
I have chosen linux as most of the servers will be running on it.

## Dependencies
```
Nginx version 1.14.0
PHP version 7.2
MySQL version 5.7
Frontend library: JQuery and bootstrap
Note: This System folder of this project must be kept in the root(/) directory.
PHP 7.2 comes with module to connect to mysql, if you are unable to connect install in manually by the following command
$ sudo apt install php7.2-mysql
```
## Installing Dependencies
```
1.Above mentioned dependencies can be installed by executing the setup script(path:/System/setup/server_setup.sh)
2.Copy server block from file(/System/setup/server_block.txt) and past them into (/etc/nginx/sites-enabled/default)
    Change domain and subdomain with your domains
3.Execute command `service nginx restart`
4.Enable remote login to your mysql database server(in case you are keeping database in seperate server)
```

## Initial setup to start the Application
```
1.Take the DB schema from file(/System/pvt/db/db_schema.sql) and execute in your database server.
2.Take the DB queries from file(/System/pvt/db/datadump.sql) and execute in your databse server.
3.Put MySql DB access credentials such as host, user, password, and database name into CONFIG array  defined in /System/config/config.php
4.The System directory of this project must be kept in the root(/) directory.
5.Create a directory named 'uploads' into /System directory of your server (uploaded files will be kept here)
6.Change ownership of the created uploads directory by executing command `chown -Rf www-data.www-data /System/uploads`
```

## How to use the Application?
```
1.Admin can access admin portal by visiting admin.gallecto.com(change it according to which you have mentioned during step 2 of installation)
2.Initial admin email ID will be 'admin@slms.com' and password will be '#welcome123'
3.As for student/user portal you can register new users and access the portal
4.In case you want to grant a user Administrator access then in DB update isAdmin field of that particular user to 1 in Users table
```

## Built with
* Web Server :Nginx(1.14.0)
* Backend Language: PHP(7.2)
* Database : MySQL(5.7)
* Frontend : JQuery(3.4), JavaScript
* Styling : Bootstrap along with  CSS

